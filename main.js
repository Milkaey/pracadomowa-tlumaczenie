
let myImage = document.querySelector('img');

myImage.onclick = function() {
  let mySrc = myImage.getAttribute('src');
  if(mySrc === 'images/firefox-icon.png') {
    myImage.setAttribute ('src','images/firefox2.png');
  } else {
    myImage.setAttribute ('src','images/firefox-icon.png');
  }
}
let myButton = document.querySelector('button');
let myHeading = document.querySelector('h1');

function setUserName() {
  let myName = prompt('Nazwa użytkownika.');
  if(!myName) {
    setUserName();
  } else {
    localStorage.setItem('name', myName);
    myHeading.innerHTML = 'Mozilla Fire fox, ' + myName;
  }
}

if(!localStorage.getItem('name')) {
  setUserName();
} else {
  let storedName = localStorage.getItem('name');
  myHeading.innerHTML = 'Mozilla fire fox, ' + storedName;
}

myButton.onclick = function() {
  setUserName();
}


var translate = {
  pl: {
    m1: "Mozilla to społeczność zajmująca się",
    m2: "Nowosciami technologicznymi",
    m3: "ułatwianiem życia",
    m4: "wszystkim co najlepsze",
    m5: "Jesteśmy zaangażowani w Internet, który promuje dyskurs obywatelski, godność ludzką i indywidualną ekspresję.",
    m6: "Przeczytaj",
    m7: "by dowiedzieć się więcej.",
    m8: "Nazwa użytkownika",
      
    // m1: "Nowosciami technologicznymi",
    // m2: "ułatwianiem życia",
    // m3: "wszystkim co najlepsze",
    // m4: "Jesteśmy zaangażowani w Internet, który promuje dyskurs obywatelski, godność ludzką i indywidualną ekspresję.",
    // m5: "Przeczytaj",
    // m6: "Nazwa użytkownika",
    // m7: "Mozilla to społeczność zajmująca się",
    // m8: " by dowiedzieć się więcej.",
  },
  en: {
    m1: "Mozilla is community that takes care of",
    m2: "Tribal news",
    m3: "Making life easier",
    m4: "All the best",
    m5: "We are committed to the Internet that promotes civic discourse, human dignity and individual expression.",
    m6: "Read on",
    m7: " to find out more",
    m8: "User Name",
    
    
    
    // m1: "Tribal news",
    // m2: "making life easier",
    // m3: "all the best",
    // m4: "We are committed to the Internet that promotes civic discourse, human dignity and individual expression.",
    // m5: "Read on",
    // m6: "User Name",
    // m7: "to find out more",
    // m8: " to find out more",
  
  }
  
};

class translator {
  constructor() {
    this.changeLanguage();
  }

  changeLanguage() {
    var i18n = translate;
    $(".language select").change(function() {
      var lang = $(this).val();
      $("#art1").text(i18n[lang].m1, 500);
      $("#art2").text(i18n[lang].m2, 500);
      $("#art3").text(i18n[lang].m3, 500);
      $("#art4").text(i18n[lang].m4, 500);
      $("#art5").text(i18n[lang].m5, 500);
      $("#art6").text(i18n[lang].m6, 500);
      $("#art7").text(i18n[lang].m7, 500);
      $("#art8").text(i18n[lang].m8, 500);
    });
  }
}

new translator()